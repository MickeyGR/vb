Imports System.Net.Http
Imports System.Text
Imports Newtonsoft.Json
Public Class UserDataAccess
    Private _client As New HttpClient()
    Private _baseUrl As String = "http://localhost:3055/api"

    Public Function UpdateUserData(usuarioId As Integer, password As String) As Usuario
        Dim user As New Usuario With {.usuarioId = usuarioId, .password = password}
        Dim json = Newtonsoft.Json.JsonConvert.SerializeObject(user)

        ' Esta línea podría ser necesaria si utilizas un APIKEY
        '_client.DefaultRequestHeaders.Add("Authorization", "Bearer " + "your_token")

       ' Se crea un objeto StringContent con la cadena JSON y se establece el tipo de contenido como "application/json"
        Dim content As New StringContent(json, Encoding.UTF8, "application/json")
        ' Se hace la solicitud PUT al servidor con la dirección "http://localhost:3055/api/v1/auth/usuarios" y el contenido creado anteriormente
        Dim result As HttpResponseMessage = _client.PutAsync( _baseUrl + "/v1/auth/usuarios", content).Result

        ' Si la solicitud fue exitosa, se lee la respuesta del servidor y se convierte a un objeto Usuario
        If result.IsSuccessStatusCode Then
            Dim jsonResponse = result.Content.ReadAsStringAsync().Result
            Dim userResponse As Usuario = JsonConvert.DeserializeObject(Of Usuario)(jsonResponse)
            ' Se retorna el objeto Usuario con la respuesta del servidor
            return userResponse
        Else
            ' Si la solicitud no fue exitosa, se lanza una excepción con un mensaje de error
            Throw New Exception("La solicitud no fue exitosa")
        End If

    End Function
End Class

Public Class Usuario
    Public Property usuarioId As Integer
    Public Property password As String
End Class
