'
Imports System.Net.Http
Imports System.Text
Imports Newtonsoft.Json
Imports UserBusiness

Module Program
    Sub Main()
        Dim userBusiness As New UserBusiness()

        ' Pedir los datos de usuarioId y contraseña al usuario por medio de la consola
        Console.WriteLine("Ingrese el usuarioId:")
        Dim usuarioId As String = Console.ReadLine()
        Console.WriteLine("Ingrese la contraseña:")
        Dim contraseña As String = Console.ReadLine()

        Try
            ' Validar y enviar los datos al servidor
            Dim updatedUser As Usuario = userBusiness.UpdateUserData(usuarioId, contraseña)
            ' Mostrar los resultados en la consola
            Console.WriteLine(updatedUser.usuarioId)
            Console.WriteLine(updatedUser.password)
        Catch ex As Exception
            Console.WriteLine("Error: " & ex.Message)
        End Try
        
        Console.ReadLine()
    End Sub
End Module

'ini - Codigo original sin capas
'Public Class Usuario
'    Public Property usuarioId As Integer
'    Public Property password As String
'End Class


'Module Program
'    Sub Main()
'        Dim client As New HttpClient()

'        Dim user As New Usuario With {.usuarioId = 1, .password = "password123"}

'        Dim json = Newtonsoft.Json.JsonConvert.SerializeObject(user)

        'esta linea va a ser necesaria cuando tenga un APIKEY
        'client.DefaultRequestHeaders.Add("Authorization", "Bearer " + "your_token")

'        Dim content As New StringContent(json, Encoding.UTF8, "application/json")
'        Dim result As HttpResponseMessage = client.PutAsync("http://localhost:3055/api/v1/auth/usuarios", content).Result

'        Console.WriteLine(json)
'        Console.WriteLine(result)
'        Console.ReadLine()

        
'    End Sub
'End Module
' fin - codigo original sin capas
