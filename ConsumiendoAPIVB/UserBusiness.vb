Imports System.Net.Http
Imports System.Text
Imports Newtonsoft.Json
Imports UserDataAccess

Public Class UserBusiness
    Private _dataAccess As New UserDataAccess()

    Public Function updateUserData(usuarioId As String, password As String) As Usuario
        ' Validar los datos de usuario y contraseña
        If Not IsValidUserData(usuarioId, password) Then
            Return Nothing
        End If

        ' Enviar los datos al servidor
        Return _dataAccess.UpdateUserData(usuarioId, password)
    End Function

    Private Function IsValidUserData(usuarioId As String, password As String) As Boolean
        ' Aquí podrías agregar las validaciones necesarias para el usuario y contraseña
        Dim userId As Integer
        If Integer.TryParse(usuarioId, userId) And Not String.IsNullOrEmpty(password) Then
           Return True
        Else
            Throw New Exception("El usuarioId debe ser un número entero y la contraseña no debe estar vacia.")
        End If
    End Function
End Class
